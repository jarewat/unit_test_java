/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.time.LocalDate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Zuper
 */
public class JobServiceTest {

    public JobServiceTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    /**
     * Test of checkEnableTime method, of class JobService.
     */
    @org.junit.jupiter.api.Test
    public void testCheckEnableTime() {
        System.out.println("checkEnableTime");
//        arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate enTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 3);
        boolean expResult = true;
//        action
        boolean result = JobService.checkEnableTime(startTime, enTime, today);
//        assert
        assertEquals(expResult, result);

    }
    @org.junit.jupiter.api.Test
    public void testCheckEnableTime2() {
        System.out.println("checkEnableTime");
//        arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate enTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 1, 31);
        boolean expResult = true;
//        action
        boolean result = JobService.checkEnableTime(startTime, enTime, today);
//        assert
        assertEquals(expResult, result);

    }
    
    @org.junit.jupiter.api.Test
    public void testCheckEnableTime3() {
        System.out.println("checkEnableTime");
//        arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate enTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 5);
        boolean expResult = true;
//        action
        boolean result = JobService.checkEnableTime(startTime, enTime, today);
//        assert
        assertEquals(expResult, result);

    }
        @org.junit.jupiter.api.Test
    public void testCheckEnableTime4() {
        System.out.println("checkEnableTime");
//        arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate enTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 1, 20);
        boolean expResult = false;
//        action
        boolean result = JobService.checkEnableTime(startTime, enTime, today);
//        assert
        assertEquals(expResult, result);

    }
        @org.junit.jupiter.api.Test
    public void testCheckEnableTime5() {
        System.out.println("checkEnableTime");
//        arrange
        LocalDate startTime = LocalDate.of(2021, 1, 31);
        LocalDate enTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 6);
        boolean expResult = false;
//        action
        boolean result = JobService.checkEnableTime(startTime, enTime, today);
//        assert
        assertEquals(expResult, result);

    }
    

}
